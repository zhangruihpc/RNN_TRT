import base64, collections, io, itertools, functools, json, os, random, re, textwrap, time, urllib, xml

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from IPython import display
import uproot
import pickle
import json
import os
import sys
import glob

from datetime import datetime

from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import os

import tensorflow as tf

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

from helpers import *


class RNN_TRT():
    def __init__(self):
        config = ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.6
        config.gpu_options.allow_growth = True
        session = InteractiveSession(config=config)       
        #final set of labels
        self.labels = ["muon", "electron"]

        # Tested with TensorFlow 2.1.0
        print('version={}, CUDA={}, GPU={}'.format(
            tf.__version__, tf.test.is_built_with_cuda(),
            # GPU attached?
            tf.test.is_gpu_available()))

        self.labels = [label.strip() for label 
            in tf.io.gfile.GFile(os.path.join(data_path, "labels.txt"))]

        counts = json.load(tf.io.gfile.GFile(os.path.join(data_path, "counts.json")))


        for record in tf.data.TFRecordDataset(tf.io.gfile.glob(
            '{}/train-*'.format(data_path))):
            first_example = tf.train.Example.FromString(record.numpy())
            break
        print('Features in example:\n\t{}'.format(
            ' '.join(first_example.features.feature.keys())))


        self.labels = [label.strip() for label in tf.io.gfile.GFile(
            '{}/labels.txt'.format(data_path))]
        counts = json.load(tf.io.gfile.GFile(
            '{}/counts.json'.format(data_path)))
        print('Labels({:d}):\n\t{}'.format(len(self.labels), self.labels))
        print('Counts:\n\t{}'.format(counts))

        # Maximum number of points in concatenated hits (exceeding discarded).
        self.MAX_LEN = 40

        # Because every drawing has a different number of points, we use `VarLenFeature`
        # and not `FixedLenFeature` for the stroke data. This will create a
        # `SparseTensor`.

        self.feature_spec ={}
        for k, vars in enumerate(hit_features_list):
            self.feature_spec[vars] = tf.io.VarLenFeature(dtype=tf.float32)
            
        for k, vars in enumerate(track_features_list):
            self.feature_spec[vars] = tf.io.FixedLenFeature([], dtype=tf.float32)
            
            
        self.feature_spec["hit_len"] = tf.io.FixedLenFeature([], tf.int64)
        self.feature_spec["label"] = tf.io.FixedLenFeature([], tf.int64)
        self.feature_spec["eProbHT"] = tf.io.FixedLenFeature([], tf.float32)
        self.steps_per_epoch = counts['train'] // args.batch_size
        self.eval_steps_per_epoch = counts['eval'] // args.batch_size
        self.test_steps_per_epoch = counts['test'] // args.test_batch_size
        self.ds_hits = self.make_ds(f'{data_path}/train-*', args.batch_size, self.parse_example_train)
        self.ds_hits_eval = self.make_ds(f'{data_path}/eval-*', args.batch_size, self.parse_example_train)

        # setup inputs
        hits = tf.keras.layers.Input(shape=(self.MAX_LEN, len(hit_features_list)), name='hits')
        track = tf.keras.layers.Input(shape=(len(track_features_list),), name='tracks')

        # add LSTM to process hits
        if(args.Bidirectional):
            lstm = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=args.lstm_hidden_size), 
                                                    input_shape=(self.MAX_LEN, len(hit_features_list)))(hits)
        else:
            lstm = tf.keras.layers.LSTM(units=args.lstm_hidden_size, input_shape=(self.MAX_LEN, len(hit_features_list)))(hits)

        # concatenate with the track inputs and feed to a dense layer
        merged = tf.keras.layers.concatenate([lstm, track])
        dense = tf.keras.layers.Dense(args.dense_size, activation='relu')(merged)

        #calculate electron probability
        e_prob = tf.keras.layers.Dense(2, activation='softmax', name='e_prob')(dense)

        self.lstm_model = tf.keras.Model(inputs=[hits, track], outputs=e_prob)
        self.lstm_model.compile(    
            optimizer=tf.keras.optimizers.Adam(args.lr_rate),
            loss='categorical_crossentropy',
            metrics=['accuracy'])
                            
        self.lstm_model.summary()   
    def parse_example_train(self, serialized_example, test = True):
        """Parses a given tf.Example and creates a dense (limited) length tensor.

        Args:
        serialized_example: tf.Example to parse.
        """
        features = tf.io.parse_single_example(serialized_example, self.feature_spec)
        label = features['label']
        track_vars = []
        for vars in track_features_list:
            track_vars.append(features[vars])
            
        eProbHT = features['eProbHT']

        hit_stack = []
        for k, vars in enumerate(hit_features_list):
            hit_stack.append(convert_sparse(features[vars], max_len=self.MAX_LEN))

        hits = tf.stack(hit_stack)

        hits = tf.transpose(hits, perm=[1, 0])

        # Also truncate the `hit_len` to MAX_LEN if needed.
        hit_len = tf.minimum(tf.cast(self.MAX_LEN, tf.int64), features['hit_len'])

        return (hits, track_vars), tf.one_hot(label, depth=len(self.labels))


    def parse_example_test(self, serialized_example):
        """Parses a given tf.Example and creates a dense (limited) length tensor.

        Args:
        serialized_example: tf.Example to parse.
        """
        features = tf.io.parse_single_example(serialized_example, self.feature_spec)
        label = features['label']
        track_vars = []
        for vars in track_features_list:
            track_vars.append(features[vars])
            
        eProbHT = features['eProbHT']

        hit_stack = []
        for k, vars in enumerate(hit_features_list):
            hit_stack.append(convert_sparse(features[vars], max_len=self.MAX_LEN))

        hits = tf.stack(hit_stack)

        hits = tf.transpose(hits, perm=[1, 0])

        # Also truncate the `hit_len` to MAX_LEN if needed.
        hit_len = tf.minimum(tf.cast(self.MAX_LEN, tf.int64), features['hit_len'])

        return (hits, track_vars), tf.one_hot(label, depth=len(self.labels)), eProbHT   

    def make_ds(self, files_pattern, batch_size, parse_example_func):
        """Converts all data within multiple TFRecord files into a
         dense (limited) length tensor format, shuffles them and creates batches.

        Args:
        files_pattern: Path with the format `[...]/train-*`.
        batch_size: Size to use for generating batches. 
        """
        dataset = tf.data.TFRecordDataset(tf.io.gfile.glob(files_pattern))
        dataset = dataset.map(parse_example_func).batch(batch_size)
        dataset = dataset.shuffle(buffer_size=5*batch_size).repeat()
        return dataset

    def train(self):
        self.lstm_model.fit(self.ds_hits,
                    validation_data=self.ds_hits_eval,
                    steps_per_epoch=self.steps_per_epoch,
                    validation_steps=self.eval_steps_per_epoch,
                    epochs=args.num_epochs,
                    verbose=args.verbose)                       

        print("Training DONE!")

        #------------------------------
        #----------TEST----------------
        #------------------------------

        print("Testing ...")

        ds_hits_test = self.make_ds(f'{data_path}/test-*', args.test_batch_size, self.parse_example_test)
        test_ds_iter = iter(ds_hits_test)

        y_scores = []; ytest = []; test_eProbHT = []
        i = 0
        for (test_hit_batch, track), label_onehot_batch, eProbHT in test_ds_iter:
            test_eProbHT += list(eProbHT.numpy())
            y_scores += list(self.lstm_model.predict([test_hit_batch, track])[:,1])
            ytest += list(tf.argmax(label_onehot_batch, axis=1).numpy())
            print(len(y_scores))
            break
        print("Test DONE!")     

    def save(self):
        keras_path = os.path.join(models_path, f'{model_name}_epoch{args.num_epochs}.h5')
        lstm_model.save(keras_path)

    def plot(self):
        #Plot ROC curve
        plot_scores(y_scores, ytest, test_eProbHT, save = args.savePlots)




