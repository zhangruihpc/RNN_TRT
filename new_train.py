import os
import sys
import numpy as np
import json
import time
from datetime import datetime
import random

from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec

import tensorflow as tf
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

from helpers import *
import ModelParameters


class RNN_TRT():

    def load_param(self, config = None):
        # Write all parameters into the dictionary self.config
        self.config = dict(ModelParameters.kDefaultConfig)
        if config is not None:            
            self.config.update(config)

        # Create variable name corresponding to keys in self.config 
        # and assign the corresponding values
        for param in self.config:
            setattr(self, param, self.config[param])

    def __init__(self, config = None):
        self._setup(config)

    def _setup(self, config = None):
        self.load_param(config)
        self.keras_path = os.path.join(self.checkpoint_dir, f'{self.model_name}_epoch{self.num_epochs}.h5')
        tf_config = ConfigProto()
        tf_config.gpu_options.per_process_gpu_memory_fraction = 0.6
        tf_config.gpu_options.allow_growth = True
        session = InteractiveSession(config=tf_config)       


        # Tested with TensorFlow 2.1.0
        print('version={}, CUDA={}, GPU={}'.format(
            tf.__version__, tf.test.is_built_with_cuda(),
            # GPU attached?
            tf.test.is_gpu_available()))

        self.labels = [label.strip() for label 
            in tf.io.gfile.GFile(os.path.join(data_path, "labels.txt"))]

        counts = json.load(tf.io.gfile.GFile(os.path.join(data_path, "counts.json")))


        for record in tf.data.TFRecordDataset(tf.io.gfile.glob(
            '{}/train-*'.format(data_path))):
            first_example = tf.train.Example.FromString(record.numpy())
            break
        print('Features in example:\n\t{}'.format(
            ' '.join(first_example.features.feature.keys())))


        self.labels = [label.strip() for label in tf.io.gfile.GFile(
            '{}/labels.txt'.format(data_path))]
        counts = json.load(tf.io.gfile.GFile(
            '{}/counts.json'.format(data_path)))
        print('Labels({:d}):\n\t{}'.format(len(self.labels), self.labels))
        print('Counts:\n\t{}'.format(counts))

        # Maximum number of points in concatenated hits (exceeding discarded).
        self.MAX_LEN = 40

        # Because every drawing has a different number of points, we use `VarLenFeature`
        # and not `FixedLenFeature` for the stroke data. This will create a
        # `SparseTensor`.

        self.feature_spec ={}
        for k, vars in enumerate(self.hit_features_list):
            self.feature_spec[vars] = tf.io.VarLenFeature(dtype=tf.float32)
            
        for k, vars in enumerate(self.track_features_list):
            self.feature_spec[vars] = tf.io.FixedLenFeature([], dtype=tf.float32)
            
            
        self.feature_spec["hit_len"] = tf.io.FixedLenFeature([], tf.int64)
        self.feature_spec["label"] = tf.io.FixedLenFeature([], tf.int64)
        self.feature_spec["eProbHT"] = tf.io.FixedLenFeature([], tf.float32)
        self.steps_per_epoch = counts['train'] // self.hp['batch_size']
        self.eval_steps_per_epoch = counts['eval'] // self.hp['batch_size']
        self.test_steps_per_epoch = counts['test'] // args.test_batch_size        
        self.ds_hits = self.make_ds(f'{data_path}/train-*', self.hp['batch_size'], self.parse_example_train)
        self.ds_hits_eval = self.make_ds(f'{data_path}/eval-*', self.hp['batch_size'], self.parse_example_train)

        # setup inputs
        hits = tf.keras.layers.Input(shape=(self.MAX_LEN, len(self.hit_features_list)), name='hits')
        track = tf.keras.layers.Input(shape=(len(self.track_features_list),), name='tracks')

        # add LSTM to process hits
        if(self.Bidirectional):
            lstm = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=self.hp['lstm_hidden_size']), 
                                                    input_shape=(self.MAX_LEN, len(self.hit_features_list)))(hits)
        else:
            lstm = tf.keras.layers.LSTM(units=self.hp['lstm_hidden_size'], 
                input_shape=(self.MAX_LEN, len(self.hit_features_list)))(hits)

        # concatenate with the track inputs and feed to a dense layer
        merged = tf.keras.layers.concatenate([lstm, track])
        dense = tf.keras.layers.Dense(self.hp['dense_size'], activation=self.hp['dense_activation'])(merged)

        #calculate electron probability
        e_prob = tf.keras.layers.Dense(2, activation='softmax', name='e_prob')(dense)

        self.lstm_model = tf.keras.Model(inputs=[hits, track], outputs=e_prob)
        self.lstm_model.compile(    
            optimizer=tf.keras.optimizers.Adam(self.hp['lr']),
            loss='categorical_crossentropy',
            metrics=['accuracy'])
                            
        self.lstm_model.summary()   
    def parse_example_train(self, serialized_example, test = True):
        """Parses a given tf.Example and creates a dense (limited) length tensor.

        Args:
        serialized_example: tf.Example to parse.
        """
        features = tf.io.parse_single_example(serialized_example, self.feature_spec)
        label = features['label']
        track_vars = []
        for vars in self.track_features_list:
            track_vars.append(features[vars])
            
        eProbHT = features['eProbHT']

        hit_stack = []
        for k, vars in enumerate(self.hit_features_list):
            hit_stack.append(convert_sparse(features[vars], max_len=self.MAX_LEN))

        hits = tf.stack(hit_stack)

        hits = tf.transpose(hits, perm=[1, 0])

        # Also truncate the `hit_len` to MAX_LEN if needed.
        hit_len = tf.minimum(tf.cast(self.MAX_LEN, tf.int64), features['hit_len'])

        return (hits, track_vars), tf.one_hot(label, depth=len(self.labels))


    def parse_example_test(self, serialized_example):
        """Parses a given tf.Example and creates a dense (limited) length tensor.

        Args:
        serialized_example: tf.Example to parse.
        """
        features = tf.io.parse_single_example(serialized_example, self.feature_spec)
        label = features['label']
        track_vars = []
        for vars in self.track_features_list:
            track_vars.append(features[vars])
            
        eProbHT = features['eProbHT']

        hit_stack = []
        for k, vars in enumerate(self.hit_features_list):
            hit_stack.append(convert_sparse(features[vars], max_len=self.MAX_LEN))

        hits = tf.stack(hit_stack)

        hits = tf.transpose(hits, perm=[1, 0])

        # Also truncate the `hit_len` to MAX_LEN if needed.
        hit_len = tf.minimum(tf.cast(self.MAX_LEN, tf.int64), features['hit_len'])

        return (hits, track_vars), tf.one_hot(label, depth=len(self.labels)), eProbHT   

    def make_ds(self, files_pattern, batch_size, parse_example_func):
        """Converts all data within multiple TFRecord files into a
         dense (limited) length tensor format, shuffles them and creates batches.

        Args:
        files_pattern: Path with the format `[...]/train-*`.
        batch_size: Size to use for generating batches. 
        """
        files = tf.io.gfile.glob(files_pattern)
        random.shuffle(files)
        dataset = tf.data.TFRecordDataset(files)
        # dataset = tf.data.TFRecordDataset(tf.io.gfile.glob(files_pattern))
        dataset = dataset.prefetch(buffer_size=batch_size*5)
        dataset = dataset.map(parse_example_func)
        dataset = dataset.shuffle(buffer_size=batch_size*5).repeat().batch(batch_size)
        return dataset

    def train(self):
        start_time = time.time()
        #keras.callbacks.tensorboard_v1.TensorBoard(log_dir='./logs', histogram_freq=0, batch_size=32, 
        #write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, 
        #embeddings_metadata=None, embeddings_data=None, update_freq='epoch')
        train_logger =  TrainLogger()
        train_logger.set_checkpoin_dir(self.checkpoint_dir)

        stopper = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min',
            patience=self.early_stopping_round)
        model_saver = tf.keras.callbacks.ModelCheckpoint(self.keras_path, save_best_only=True, monitor='val_loss', mode='min')
        #reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=1, epsilon=1e-4, mode='min')
        self.lstm_model.fit(self.ds_hits,
                    validation_data=self.ds_hits_eval,
                    steps_per_epoch=self.steps_per_epoch,
                    validation_steps=self.eval_steps_per_epoch,
                    epochs=self.num_epochs,
                    verbose=self.verbose,
                    callbacks=[train_logger, stopper, model_saver])                       

        print("Training DONE!")

        #------------------------------
        #----------TEST----------------
        #------------------------------

        
        accuracy = self.test()

        end_time = time.time()
        total_time = end_time - start_time
        print("Test score: {}".format(accuracy))
        print("Time: {}s".format(total_time))
        with open('output.json', 'w') as f:
            scores = {'loss': -float(accuracy)}
            json.dump(scores, f)

        metadata = {'hyperparameters': self.hp}
        metadata['metric'] = 'accuracy'
        metadata['accuracy'] = str(accuracy)
        metadata['site'] = self.site
        metadata['total_time'] = str(total_time)
        json.dump(metadata, open("userJobMetadata.json",'w'))
        
        return {"mean_accuracy": accuracy}   
    def test(self):
        ds_hits_test = self.make_ds(f'{data_path}/test-*', self.test_batch_size, self.parse_example_test)

        _, accuracy = self.lstm_model.evaluate(ds_hits_test, steps=self.test_steps_per_epoch,
                    verbose=0)  
        return accuracy

    def load(self, path):
        self.lstm_model.load(path)
        
    def plot(self):
        #Plot ROC curve
        plot_scores(y_scores, ytest, test_eProbHT, save = self.savePlots)


class TrainLogger(tf.keras.callbacks.Callback):
    def set_checkpoin_dir(self, checkpoint_dir):
        self.checkpoint_dir = checkpoint_dir
    def on_train_begin(self, logs = {}):
        self.total_time = 0
        os.makedirs(self.checkpoint_dir, exist_ok = True)
        self.log = { 'train_acc' : [], 'train_loss': [],
        'val_acc': [], 'val_loss': [], 'time': []}
    def on_epoch_begin(self, epoch, logs = {}):
        self.start_time = time.time()
    def on_epoch_end(self, epoch, logs = {}):
        self.end_time = time.time()
        time_diff = self.end_time - self.start_time
        self.total_time += time_diff
        self.log['time'].append(float(time_diff))
        self.log['total_time'] = float(self.total_time)
        self.log['train_acc'].append(float(logs.get('accuracy')))
        self.log['train_loss'].append(float(logs.get('loss')))
        self.log['val_acc'].append(float(logs.get('val_accuracy')))
        self.log['val_loss'].append(float(logs.get('val_loss')))    
        json.dump(self.log,open(self.checkpoint_dir + "/train_log.json",'w'))
        json.dump(self.log,open("userJobMetadata.json",'w'))
    #def on_batch_end():
    #def on_epoch_start():
    #def on_train_end():

