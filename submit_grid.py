import os
import glob
import argparse
import json
from ray import tune
from ray.tune.suggest.variant_generator import generate_variants,flatten_resolved_vars
from pdb import set_trace
import numpy as np

import ModelParameters

'''

Currently working grid site:
ANALY_MANC_GPU_TEST
ANALY_QMUL_GPU_TEST
ANALY_MWT2_GPU
ANALY_BNL_GPU_ARC
'''
parser = argparse.ArgumentParser()
parser.add_argument('--config', action='store', help='base configuration file to use', default = 'config.json')
parser.add_argument('--docker', action='store', help='docker image to use', default = \
	'/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/clcheng/hyperparameter-optimization-on-the-grid:latest')
parser.add_argument('--site', action='store', help='grid site to use', default = \
	'ANALY_MANC_GPU_TEST')
parser.add_argument('--dataset', action='store', help='name of rucio dataset', default = \
	'user.${RUCIO_ACCOUNT}:user.${RUCIO_ACCOUNT}.RNNTRT.v02.dataset')
parser.add_argument('--output', action='store', help='name of output dataset', default = \
	'user.${RUCIO_ACCOUNT}.RNNTRT.HPOGRID.01.$(date +%Y%m%d%H%M%S)')
parser.add_argument('--n_point', action='store', help='number of search points', type = int, default = 8)
parser.add_argument('--max_gpu', action='store_true', help='maximize the number of gpu used', default = False)

args = parser.parse_args()



site_ngpu = {
	'ANALY_MANC_GPU_TEST': 10, #single queue, no submission parameters, 1 GPU per job
	'ANALY_QMUL_GPU_TEST': 6, #	GPUNumber=x for now is hardcoded in the dev APF JDL,number of GPUs per job limited by cgroups, K80=2*K40, so total of 6 gpu slots avalable.
	'ANALY_MWT2_GPU': 8, #single queue, no submission parameters, 1 GPU per job
	'ANALY_BNL_GPU_ARC': 12, #also shared with Jupyter users who have priority
	'ANALY_INFN-T1_GPU': 2 #single queue, no submission parameters, 1 GPU per job
}

def convert(o):
    if isinstance(o, np.int64): return int(o)  
    raise TypeError

def main(args):
	#"lr": tune.loguniform( 1e-6, 0.0005),
	search_space = \
		{
			"batch_size": tune.choice([128, 256, 512, 1024, 2048, 4096, 8192]),
			"lr": tune.loguniform( 1e-5, 1e-1),
			"lstm_hidden_size":  tune.choice([8, 16, 32, 64, 128, 256]),
			"dense_size": tune.choice([8, 16, 32, 64, 128, 256]),
			"dense_activation": tune.choice(['relu','elu','softmax', 'sigmoid', 'tanh'])
		}
	num_samples = args.n_point

	os.makedirs('config_files', exist_ok=True)
	for i in range(num_samples):
		for resolved_vars, _ in generate_variants(search_space):
			hparams = flatten_resolved_vars(resolved_vars)
			print('Using hyperparameters: {}'.format(hparams))
			config = json.load(open(args.config))
			config['hp'].update(hparams)
			config['site'] = args.site
			json.dump(config, open(args.config, 'w'), default=convert)
			if args.site == 'None':
				site = ''
			else:
				if 'GPU' in args.site:
					site = '--cmtConfig nvidia-gpu --site {}'.format(args.site)
				else:
					site = '--site {} --nCore 8'.format(args.site)
			command = \
			'prun --containerImage \
			{} \
			--exec "PATH=/opt/conda/envs/ml-base/bin && \
			python TestRun.py --config_file {}" \
			--inDS {} \
			--outDS {} \
			{} \
			--forceStaged \
			--useSandbox \
			--noBuild \
			--ctrWorkdir /ctrdata \
			--ctrDatadir /ctrdata \
			--disableAutoRetry'.format(args.docker, args.config, args.dataset, args.output, site)
			os.system(command)

if __name__ == '__main__':
	main(args)
