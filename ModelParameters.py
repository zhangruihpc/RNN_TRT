kDefaultConfig = {
    'num_epochs' : 40,
    'savePlots' : 1,
    'modelSave' : 1,
    'batch_size' : 100,
    'test_batch_size' : 10000,
    'Bidirectional' : 0,
    'verbose' : 0,
    'isTrain' : 1,
    'checkpoint_dir': './checkpoints',
    #final set of labels
    'labels' : ["muon", "electron"],
    'model_name' : "TRT_TF_v1",
    'description' : "TF run with hit + track variables - weighted sample",
    'dataset_name' : "trt_sharded_weighted_1M5K",
    #list of selected track features to be used in the model
    'track_features_list' : ["trkOcc", "p", "pT", "nXehits", "fAr", "fHTMB", "PHF", "dEdx"],
    #list of selected hit features to be used in the model
    'hit_features_list' : ["hit_HTMB", "hit_gasType", "hit_tot", "hit_L", "hit_rTrkWire", "hit_HitZ", "hit_HitR"],
    'hp':{
        'lr': 0.0001,
        'lstm_hidden_size' : 128,
        'dense_size' : 128,        
        'dense_activation': 'relu',
        'batch_size': 1000
    },
    'site': '',
    'early_stopping_round': 10,
    'grid_job': 0
}