import base64, collections, io, itertools, functools, json, os, random, re, textwrap, time, urllib, xml

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from IPython import display
import uproot
import pickle
import sys
import glob
from functools import reduce
from datetime import datetime

from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from config import *

import tensorflow as tf

workdir = os.getcwd()

models_path = f"{workdir}/models/"
plots_path = f"{workdir}/plots/"

data_path = os.path.join(workdir, dataset_name)

try:
    os.mkdir(models_path)
except OSError:
    print ("Creation of the model directory failed - probably it already exists...")
else:
    print ("Successfully created the model directory ")

try:
    os.mkdir(plots_path)
except OSError:
    print ("Creation of the model directory failed - probably it already exists...")
else:
    print ("Successfully created the model directory ")


args = args()

def findFiles(path): 
    """Finds all of the file in a directory whose path is given
    """
    return glob.glob(path)

def plot_roc_curve(fpr, tpr, label = None, color = "C0", line='-'):
    plt.plot(tpr, fpr, linewidth = 2, label= label, color = color, linestyle=line)
    plt.axis([0,1,0,1])
    plt.xlabel("Signal Efficiency")
    plt.ylabel("Background Efficiency")


def plot_scores(y_scores, ytest, test_eProbHT, save = False):
	e_y_scores = [y_scores[i] for i in range(len(y_scores)) if ytest[i] == 1]
	e_test_eProbHT = [test_eProbHT[i] for i in range(len(test_eProbHT)) if ytest[i] == 1]
	m_y_scores = [y_scores[i] for i in range(len(y_scores)) if ytest[i] == 0]
	m_test_eProbHT = [test_eProbHT[i] for i in range(len(test_eProbHT)) if ytest[i] == 0]
	print(f"# electrons: {len(e_y_scores)} - #muons: {len(m_y_scores)}")

	fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(18,6))

	fpr, tpr, thresholds = roc_curve(ytest, y_scores)
	test_auc = roc_auc_score(ytest, y_scores)
	ax1.plot(tpr, fpr, linewidth = 2, label= f"RNN ({np.round(test_auc,4)})", color = "C0")
	fpr, tpr, thresholds = roc_curve(ytest, test_eProbHT)
	test_eProbHT_auc = roc_auc_score(ytest, test_eProbHT)
	ax1.plot(tpr, fpr, linewidth = 2, label= f"eProbHT ({np.round(test_eProbHT_auc,4)})", color = "C1")
	ax1.axvline(x=0.9,linestyle='--',color='k', label = "%90 eff.")
	ax1.legend(fontsize = 14, markerscale = 2, loc = 'best')

	n1, bins, patches = ax2.hist(e_y_scores, 50, weights=np.ones_like(e_y_scores) / len(e_y_scores), color = "C0", label = "Electron RNN scores", alpha = 0.5)
	ax2.hist(e_test_eProbHT, 50, weights=np.ones_like(e_test_eProbHT) / len(e_test_eProbHT), color = "C1", label = "Electron eProbHT scores", alpha = 0.5)
	ax2.legend(fontsize = 14, markerscale = 2, loc = 'best')
	ax2.set_xlabel("Scores")

	n2, bins, patches = ax3.hist(m_y_scores, 49, weights=np.ones_like(m_y_scores) / len(m_y_scores), color = "C0", label = "Muon RNN scores", alpha = 0.5)
	ax3.hist(m_test_eProbHT, 49, weights=np.ones_like(m_test_eProbHT) / len(m_test_eProbHT),  color = "C1", label = "Muon eProbHT scores", alpha = 0.5)
	ax3.legend(fontsize = 14, markerscale = 2, loc = 'best')
	ax3.set_xlabel("Scores")
	fig.suptitle(f"Score distributions (normalized) and ROC curve")
	if save: plt.savefig(f"{plots_path}ROC_curve_scores_{model_name}_epoch{args.num_epochs}.png")
	#plt.show()

def convert_sparse(sparse, max_len):
	"""Converts batched sparse tensor to dense tensor with specified size.

	Args:
	sparse: tf.SparseTensor instance of shape=[n].
	max_len: Truncates / zero-pads the dense tensor to have a length equal to 
	    this value.
	"""
	# Convert to dense tensor.
	dense = tf.sparse.to_dense(sparse)
	# Discard values above `max_len`.
	dense = dense[:max_len]
	# Zero-pad if `length` < `max_len`.
	dense = tf.pad(dense, [[0, max_len - tf.shape(dense)[0]]])
	return dense

