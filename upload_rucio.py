import os
import argparse
from pdb import set_trace
from time import gmtime, strftime
import tarfile

import ModelParameters

#time = strftime("%Y%m%d%H%M%S", gmtime())
parser = argparse.ArgumentParser()
parser.add_argument('--datapath', action='store', help='path of csv voxalisation files', default =\
 					'/afs/cern.ch/work/c/chlcheng/Repository/development/projects/trt_sharded_weighted_1M5K')
parser.add_argument('--tarpath', action='store', help='path of csv voxalisation files', default =\
 					'/afs/cern.ch/work/c/chlcheng/Repository/development/projects/')
args = parser.parse_args()



def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))


def main(args):
	DISK= 'UKI-NORTHGRID-MAN-HEP_SCRATCHDISK'
	#'BNL-OSG2_SCRATCHDISK' 
	did_prefix = ModelParameters.Prefix_To_Avoid_DID_Conflict
	DATASET='user.${{RUCIO_ACCOUNT}}.RNNTRT.{}dataset'.format(did_prefix)
	os.system('rucio add-dataset {}'.format(DATASET))
	tarfilepath = os.path.join(args.tarpath, os.path.basename(args.datapath))+'.tar.gz'
	#make_tarfile(tarfilepath,args.datapath)
	tarfilename = os.path.basename(tarfilepath)
	os.system('rucio upload '+tarfilepath+' --name {}{}'.format(did_prefix,tarfilename)+' --rse '+DISK)
	os.system('rucio attach '+DATASET+' user.${RUCIO_ACCOUNT}:'+'{}{}'.format(did_prefix,tarfilename))

if __name__ == '__main__':
    main(args)
