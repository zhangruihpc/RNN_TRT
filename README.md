# TRT RNN Hyperopt

TRT RNN (priliminary) Implementation in TF v2.1 for hyperoptimisation

## Usage

Make sure you have the 'dataset' folder in the same folder where this code resides.
Then just run 'python3 train.py'.

## Requirements
Tensorflow 2.1
