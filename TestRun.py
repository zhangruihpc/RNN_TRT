
import numpy as np
import os, sys
import shutil

import argparse
import json
import tarfile

##########################
# Custom Library
from new_train import RNN_TRT
################################


parser = argparse.ArgumentParser()
parser.add_argument('--config_file', action='store', help='input configuration', required=False)
args = parser.parse_args()

default_config = {
        'lstm_hidden_size' : 128,
        'dense_size' : 128,
        'lr_rate' : 0.0001,
        'num_epochs' : 40,
        'savePlots' : 1,
        'modelSave' : 1,
        'batch_size' : 100,
        'test_batch_size' : 10000,
        'Bidirectional' : 0,
        'verbose' : 1,
        'isTrain' : 1,
        #final set of labels
        'labels' : ["muon", "electron"],
        'model_name' : "TRT_TF_v1",
        'description' : "TF run with hit + track variables - weighted sample",
        'dataset_name' : "trt_sharded_weighted_1M5K",
        #list of selected track features to be used in the model
        'track_features_list' : ["trkOcc", "p", "pT", "nXehits", "fAr", "fHTMB", "PHF", "dEdx"],
        #list of selected hit features to be used in the model
        'hit_features_list' : ["hit_HTMB", "hit_gasType", "hit_tot", "hit_L", "hit_rTrkWire", "hit_HitZ", "hit_HitR"],
        'grid_job': 0
    }

def extract_inputDS():
    tarfiles = [ f for f in os.listdir('/ctrdata/') if f.endswith('tar.gz')]
    inputDS = []
    for f in tarfiles:
        tar = tarfile.open(f, "r:gz")
        print('untaring the file {}'.format(f))
        tar.extractall()
        inputDS = tar.getnames()
        tar.close()

    return inputDS

def remove_inputDS(inputDS):
    for ds in inputDS:
        if os.path.isfile(ds):
            os.remove(ds)
        elif os.path.isdir(ds):
            shutil.rmtree(ds)

def main(args):
    config = json.load(open(args.config_file))
    if config['grid_job']:
        inputDS = extract_inputDS()
    rnn_trt = RNN_TRT(config)
    rnn_trt.train()
    if config['grid_job']:
        remove_inputDS(inputDS)

if __name__ == '__main__':
    main(args)
