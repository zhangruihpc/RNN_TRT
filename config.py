isTrain = True #Is it training run? - set to True if so

model_name = "TRT_TF_v1"
description = "TF run with hit + track variables - weighted sample"

#dataset_name = "trt_sharded_weighted_1M5K"

dataset_name = "trt_sharded_weighted_1M5K"

#list of selected track features to be used in the model
track_features_list = ["trkOcc", "p", "pT", "nXehits", "fAr", "fHTMB", "PHF", "dEdx"]

#list of selected hit features to be used in the model
hit_features_list = ["hit_HTMB", "hit_gasType", "hit_tot", "hit_L", "hit_rTrkWire", "hit_HitZ", "hit_HitR"]


class args(object):
	def __init__(self):
		self.lstm_hidden_size = 128
		self.dense_size = 128
		self.lr_rate = 0.0001
		self.num_epochs = 40
		self.savePlots = True
		self.modelSave = True
		self.batch_size = 100
		self.test_batch_size = 10000
		self.Bidirectional = False
		self.verbose = True
